# cuemaker

A python script for turning YouTube descriptions into CUE files from the command line. 

**Usage**

Copy and paste the list of timestamps and song titles into an empty text document, then run the program using:
`python3 cuemaker.py "description.txt" "An Album" "A. Performer"`. 

By default the Regex pattern expects the timestamps to be in the format of `[00:00] A song title` or `00:00 - A song title`. If your timestamps are in a different format, you can supply a custom format using the `--pattern` optional argument. If you do, make sure to supply the new group numbers for the hour, minute, seconds, and title groups. Make sure to -1 from the group number as the groups are converted to a python list which starts counting at 0 rather than regex's 1. For help with regex groups, experiment here: [regexr](https://regexr.com/) using the default pattern of `"(\[)?((\\d{1,2}):)?(\\d{1,2}):(\\d{1,2})(\])? (.*)"`. Default parameters for hr, m, s, and title are: hr=2, m=3, s=4, title=6.

